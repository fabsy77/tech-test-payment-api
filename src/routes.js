import { Router } from "express";

const routes = new Router();

const vendasdb = [];

let dataOrder = {
    "venda":"Venda realizada com sucesso",
    "details":{
        "id":"",
        "name":"",
        "cpf":"",
        "email":"",
        "payment_value":"",
        "date_created":"",
        "date_update":"",
        "itens":[
            {
                "itens":""
            }
        ]
    }, 
    "status": "pagamento Ok"
};

routes.get('/',(req,res)=>{
    res.render("index");
})
routes.get('/docs',(req,res)=>{
    res.render("doc.ejs");
})
routes.post('/register',(req,res)=>{
    let venda = req.body.venda;
    let status = req.body.status;
    let payment_value = req.body.payment_value;
    let id = req.body.id;
    let name = req.body.name;
    let cpf = req.body.cpf;
    let email = req.body.email;
    let date_created = req.body.date_created;
    let date_update = req.body.date_update;
    let itens = req.body.itens;
     if(itens != ""){
        dataOrder.venda = venda;
        dataOrder.status = status;
        dataOrder.details.payment_value = payment_value;
        dataOrder.details.id = id;
        dataOrder.details.name = name;
        dataOrder.details.cpf = cpf;
        dataOrder.details.email = email;
        dataOrder.details.date_created = date_created;
        dataOrder.details.date_update = date_update;
        dataOrder.details.itens[0].itens = itens;
        vendasdb.push(dataOrder)
        res.json(dataOrder);

    }else{
        res.json({"status":"erro cod 1 preencha os campos corretamente"})
    }
    res.json(status);
})

routes.post('/getorderid',(req,res)=>{
    let {id} = req.body;
    try {
        res.json(vendasdb[id])        
    } catch (error) {
        res.json({"status":"informe um id válido"})
    }

})

routes.post('/updateorderid',(req,res)=>{
    let venda = req.body.venda;
    let status = req.body.status;
    let payment_value = req.body.payment_value;
    let id = req.body.id;
    let name = req.body.name;
    let cpf = req.body.cpf;
    let email = req.body.email;
    let date_created = req.body.date_created;
    let date_update = req.body.date_update;
    let itens = req.body.itens;

    if(vendasdb[id].status == "Aguardando Pagamento"){
        if(status != "Pagamento Aprovado" || status != "Pagamento Cancelado"){
            res.status(500).json({"status":"Não foi possivel completar a operação"});
        }else{
            vendasdb[id].venda = venda;
            vendasdb[id].status = status;
            vendasdb[id].details.payment_value = payment_value;
            vendasdb[id].details.id = id;
            vendasdb[id].details.name = name;
            vendasdb[id].details.cpf = cpf;
            vendasdb[id].details.email = email;
            vendasdb[id].details.date_created = date_created;
            vendasdb[id].details.date_update = date_update;
            vendasdb[id].details.itens[0].itens = itens;
            res.status(200).json(vendasdb[id]);
        }
    }else if(vendasdb[id].status == "Pagamento Aprovado"){
        if(status != "Pagamento Pendente"){
            res.status(500).json({"status":"Esta venda já foi paga"});
        }else if(vendasdb[id].status == "cancelado"){
            res.status(500).json({"status":"Este pedido já foi cancelado"});
        }
        else{
            vendasdb[id].venda = venda;
            vendasdb[id].status = status;
            vendasdb[id].details.payment_value = payment_value;
            vendasdb[id].details.id = id;
            vendasdb[id].details.name = name;
            vendasdb[id].details.cpf = cpf;
            vendasdb[id].details.email = email;
            vendasdb[id].details.date_created = date_created;
            vendasdb[id].details.date_update = date_update;
            vendasdb[id].details.itens[0].itens = itens;
            res.status(200).json(vendasdb[id]);
        }
    }else if(vendasdb[id].status == "Enviado para Transportador"){
        if(status != "entregue" || status != "cancelado"){
            res.status(500).json({"status":"Este pedido já foi despachado"});
        }else{
            vendasdb[id].venda = venda;
            vendasdb[id].status = status;
            vendasdb[id].details.payment_value = payment_value;
            vendasdb[id].details.id = id;
            vendasdb[id].details.name = name;
            vendasdb[id].details.cpf = cpf;
            vendasdb[id].details.email = email;
            vendasdb[id].details.date_created = date_created;
            vendasdb[id].details.date_update = date_update;
            vendasdb[id].details.itens[0].itens = itens;
            res.status(200).json(vendasdb[id]);
        }
    }

   
})

export default routes;


//A API deve possuir 3 operações:
//Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento"; feito
//Buscar venda: Busca pelo Id da venda; feito
/*Atualizar venda: Permite que seja atualizado o status da venda. feito
OBS.: Possíveis status: Pagamento aprovado | Enviado para transportadora | Entregue | Cancelada.*/
//Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos; feito
//O vendedor deve possuir id, cpf, nome, e-mail e telefone; feito
//A inclusão de uma venda deve possuir pelo menos 1 item; feito
/*A atualização de status deve permitir somente as seguintes transições:

De: Aguardando pagamento Para: Pagamento Aprovado feito

De: Aguardando pagamento Para: Cancelada feito

De: Pagamento Aprovado Para: Enviado para Transportadora

De: Pagamento Aprovado Para: Cancelada feito

De: Enviado para Transportador. Para: Entregue*/